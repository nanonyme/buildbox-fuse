#!/bin/bash
set -ex
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
t=$(mktemp --dir)
mkdir -p "$t"/cache/
cp -r "$DIR"/objects "$t"/cache/objects

mkdir -p "$t"/mount
buildbox-fuse --local="$t"/cache --input-digest-value="19796dd6150649962f6cea1d3ce43eec7a866dc3bb7c70cc0891618223469411/75" "$t"/mount &
sleep 1
ls "$t"/mount/foo
